# 开放麒麟(openKylin)-1.0-Beta 版本公测-测试方案	

## 一、版本情况	

### 1 版本下载地址
[点击下载](https://factory.openkylin.top/kif/build/view/506)  

[百度网盘](https://pan.baidu.com/s/1vb1m3om625UEXZJVzq5MUg?pwd=1234)

### 2 版本内容

 1、新增需求
1.  搭载6.1+5.15双内核
2.  核心组件的自主选型升级
3.  UKUI 4.0优化
4.  新增智能语音助手
5.  新增Win应用支持
6.  新增备份还原工具

2、修复issue（详见任务列表“已验收”状态issue）


## 二、测试计划

### 1 测试环境
#### 硬件环境
 1.包括X86实体机及虚拟机（Linux\win+VMware、virtualbox）

 2.开发板适配测试及特殊机型测试及缺陷验证需要使用指定设备，其他无要求
 
 #### 软件环境
 1.系统支持PC平板二合一，可在PC平板两种模式下进行验证

 2.系统支持6.1+5.15双内核，可在安装时进行选择（系统安装完后如果需要切换5.15内核，请在grub页面进入“openKylin GNU/Linux的高级选项”选择）
 
 3.包括wayland及X两种模式，默认进入X模式
 
 4.镜像直接安装

### 2 测试策略

1.版本测试重点：重点对于新增功能、回归issue的针对性测试

2.虚拟机支持：支持Linux\win+VMware、virtualbox及增强功能等


### 3 测试时间

5月16日--6月6日

### 4 测试内容

1.系统全盘测试，重点为新增内容试用测试，包括搭载6.1+5.15双内核、核心组件的自主选型、UKUI 4.0优化、智能语音助手、Win应用支持、新增备份还原工具等多个需求

任意软件功能问题、设计缺陷问题、UI显示问题、易用性问题、性能/稳定性问题或者第三方软件问题等，均可进行验证反馈

2.版本已解决issue验证，重点验证issue解决及是否有新问题引入（详见任务列表“已验收”状态issue）

### 5 issue提交
规范可参考
[openKylin测试参与指南](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%89%88%E6%9C%AC%E6%B5%8B%E8%AF%95%E5%8F%82%E4%B8%8E%E6%8C%87%E5%8D%97.md) 及
[openKylin 缺陷issue处理流程](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%BC%BA%E9%99%B7%E5%A4%84%E7%90%86%E6%B5%81%E7%A8%8B.md)

1.本次版本为公测版本，存在部分已知问题正在修复中，提交前注意查找已有issue，避免重复
所有[已知issue](https://gitee.com/organizations/openkylin/issues?assignee_id=&author_id=&branch=&collaborator_ids=&issue_search=&label_ids=&label_text=&milestone_id=&priority=&private_issue=&program_id=&project_id=&project_type=&scope=&single_label_id=&single_label_text=&sort=&state=progressing&target_project=) 

可见 gitee-openKylin-【任务】-“进行中”，已知缺陷不需要重复提交；

（点openkylin-任务，找到对应组件，即可到组件所有issue页面，在搜索框输入标题或者ID都可以找到对应issue）

2.issue提交在各模块的仓库下，如【ukui-control-center】

3.仅wayland模式下发现的issue，提交时标题带有【wayland】，可切换X下对比确认

4.平板模式发现的issue，标题带有【平板模式】

5.在issue中提交具体的复现手法（测试工具）及硬件机型

### 6 issue验证
涉及到具体架构或机型的缺陷，须到对应的架构或机型上验证，验证完成后进行评论备注。
详细流程见文档[openKylin缺陷处理流程](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%BC%BA%E9%99%B7%E5%A4%84%E7%90%86%E6%B5%81%E7%A8%8B.md)


### 7 风险评估

测试主机设备及硬件设备覆盖型号无法确保完全，存在未覆盖型号无法确认是否支持的风险


##  二、贡献评判

### 评判依据

1. 提交有效issue数量：

    包括完整信息的issue为有效issue。
    问题描述不清，问题与系统不相关，或者同一账号提交完全相同问题等情况，属于无效问题，不计入提交issue数量；
    有效问题数量越高，得分越高；

2. 提交信息完整性：

    issue需要标题能概括问题，易于理解，内容详细且清晰地描述问题现象、复现步骤及环境信息。避免存在描述不清或者复现步骤不全，导致问题无法定位；
    需要覆盖【标题描述】、【环境信息】、【问题复现步骤】、【预期结果】、【实际结果】、【附件信息】
    信息覆盖条数越多，得分越高；

3. 是否已知：

    未提交过的新问题更能促进版本质量提升；
    非已知问题得分更高；

4. 等级：

    根据问题严重性判断；
    反馈问题越严重，得分越高；

---

### 评判规则

<br>


根据issue提交数量及质量评判总体贡献度：

$$S=b\times n +c\times n\times 5 +d\times n\times 2 $$

- S: 贡献积分
- b：提交信息完整性的分数，分值[0,3]；
- c：是否已知的分数，分值[1,2]；
- d：等级的分数，分值[1,4]；
- n：有效问题数量；

  <br>

详细可参考[openKylin测试贡献评审规则](https://gitee.com/openkylin/qa/edit/master/%E6%B5%8B%E8%AF%95%E8%B4%A1%E7%8C%AE%E8%AF%84%E5%AE%A1%E8%A7%84%E5%88%99.md)