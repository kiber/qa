# openkylin-qa

## Description
openKylin QA 

Committed to improving the quality of openkylin community version, including community version testing and quality assurance.


The main quality assurance aspects involved in QA:


- Develop test participation guidelines and review rules to enable more community developers to use, feedback, optimize and contribute.

- Develop test tools to improve code development efficiency and test verification efficiency.

- Conduct the activities related to operational testing, including testing of various versions and important components.

## Main Content

- Test related document guidance
- Known problems

## SIG Members

SIG-owner

- 唐晓东

SIG-maintainer

- liyuan_yuan(liyuanyuan@kylinos.cn)
- fanwei1203(fanwei@kylinos.cn)

## email

qa@lists.openkylin.top
