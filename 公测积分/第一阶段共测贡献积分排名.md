## 测试贡献积分排名

7月22日-9月6日测试贡献积分排名

积分计算规则见：

[测试贡献评审规则](https://gitee.com/openkylin/community/blob/master/sig/qa/%E6%B5%8B%E8%AF%95%E8%B4%A1%E7%8C%AE%E8%AF%84%E5%AE%A1%E8%A7%84%E5%88%99.md)



|缺陷创建者|gitee ID|有效issue数量|贡献值|
|---------|-----------|-------|-------|
|	幻想sk.stu	|	sk-stu	|	7	|	44	|
|	华仔	|	hhw-baobei	|	7	|	41	|
|	凛葵依	|	LinKuiyi	|	7	|	40	|
|	谭承浩	|	tan-chenghao	|	5	|	34	|
|	GpMelon	|	gpmelon	|	3	|	17	|
|	yizimi远欣	|	yizimi-yuanxin	|	2	|	13	|
|	lerambo	|	lerambo	|	2	|	12	|
|	hoverload	|	hoverload	|	2	|	11	|
|	wongweemin	|	wongweemin	|	2	|	10	|
|	wongweemin	|	wongweemin	|	2	|	10	|
|	xp.李	|	zhaozhaoren	|	1	|	8.5	|
|	千影	|	chikage99	|	1	|	7	|
|	shaoxia	|	cheng_long88	|	1	|	7	|
|	okmyapp	|	okmyapp	|	1	|	7	|
|	高心谨	|	heart_and_caution	|	1	|	7	|
|	rtlhq	|	rtlhq	|	1	|	7	|
|	gaoyanglinux	|	gaoyanglinux	|	1	|	6.5	|
|	zhangtianxiong	|	kiber	|	1	|	6	|
|	沧海人间	|	the-sea-and-the-world	|	1	|	6	|
|	starsky	|	zcfsky	|	1	|	6	|
|	王芮	|	wr-love	|	1	|	6	|
|	Steins;Gate	|	ayibin	|	1	|	5.5	|
|	meiyujack	|	meiyujack	|	1	|	5	|
|	Jasson Chow	|	jasson-chow	|	1	|	5	|
|	张名实	|	sunset-dreamset	|	1	|	5	|
|	 **总计** 	|		|	54	|		|
